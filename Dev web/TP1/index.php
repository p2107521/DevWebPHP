<?php include('modules/head.php'); ?>

<body>
    <?php include('modules/header.php'); ?>
    <main>
        <section>
            <img src="img/table1.jpg" alt="Mon image">
            <p>Lorem ipsum dolor sit amet consectetur adipisici</p>
        </section>
        <section>
            <img src="img/table1.jpg" alt="Mon image">
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Magni veniam assumenda, voluptatum maxime dolor eaque.</p>
        </section>
        <section>
            <img src="img/table1.jpg" alt="Mon image">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam, praesentium magnam!</p>
        </section>
        <section>
            <img src="img/table1.jpg" alt="Mon image">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis, dolor!</p>
        </section>
        <section>
            <img src="/img/table1.jpg" alt="Mon image">
            <p>Lorem ipsum dolor sit.</p>
        </section>
    </main>
    <?php include('modules/footer.php'); ?>
</body>

</html>
